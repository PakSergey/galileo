<!DOCTYPE html>
<html>
<head>
    <title>Учебный центр Галилео. Подготовка к ЕГЭ и ОГЭ в городе Артёме. Математика, физика, русский язык, химия, биология</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="Подготовка к ЕГЭ (11 кл.) и ОГЭ (9 кл.) по всем предметам школьной программы (математика, физика, русский язык, литература, история, обществознание)">
    <meta name="Keywords" content="Галилео, учебный центр, ЕГЭ город Артем">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Pacifico" rel="stylesheet">
    <link href="css/bootstrap.min.css" type="text/css" rel = "stylesheet"/>
    <link href="css/bootstrap-theme.min.css" type="text/css" rel = "stylesheet"/>
    <link href="css/bootstrap.min.css.map" type="text/css" rel = "stylesheet"/>
    <link href="css/style.css" type = "text/css" rel = "stylesheet"/>
</head>
<body>
<?php include_once ROOT . "/views/templates/header.php"?>

<div class="container content">
    <div class="row usluga" style="text-align: left">
        <div class="zagolovok"><h1>Подготовка к сдаче ЕГЭ и ОГЭ по всем предметам школьной программы</h1></div>
        <div class="col-md-6">
            <img src="img/ege-desc.jpg">
        </div>
        <div class="col-md-6">
            <p>
                Успешная сдача единого государственного экзамена (ЕГЭ) — это не только положительный итог учебы в средней школе, но и залог поступления в желаемый вуз, ведь именно высокий балл, полученный на ЕГЭ, является возможностью стать студентом престижного университета.
            </p>
            <p>
                Наш центр реализует подготовку к ЕГЭ и ОГЭ по следующим предметам:
            </p>
            <ul>
                <li>математика</li>
                <li>физика</li>
                <li>русский язык</li>
                <li>химия</li>
                <li>биология</li>
            </ul>
        </div>
    </div>
    <div class="row desc-info-block">
        <div class="col-md-4">
            <img src="img/ruble.png">
            <h3>Стоимость</h3>
            <p>3000 рублей за 8 занятий</p>
        </div>
        <div class="col-md-4">
            <img src="img/clock.png">
            <h3>Продолжительность</h3>
            <p>1 час 30 минут с 10-ти минутным перерывом на кофе-брейк</p>
        </div>
        <div class="col-md-4">
            <img src="img/info.png">
            <h3>Дополнительно</h3>
            <p>Занятия ведутся 2 раза в неделю. Занятия проходят в малых группах (от 2-х человек). Оплата производится за фактически посещенные занятия. Пропущенные занятия ученик не оплачивает.</p>
        </div>
    </div>
</div>

<?php include_once ROOT . "/views/templates/enrollment-block.php"?>

<?php include_once ROOT . "/views/templates/footer.php"?>

</body>
</html>


