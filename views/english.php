<!DOCTYPE html>
<html>
<head>
    <title>Английский язык для дошкольников и учащихся начальной школы в городе Артёме</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="С каждым годом повышается роль английского языка в современном мире. И чтобы лучше овладеть навыками в его обучении, лучше начать его изучение с младшего дошкольного возраста. ">
    <meta name="Keywords" content="Галилео, учебный центр, ЕГЭ город Артем">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Pacifico" rel="stylesheet">
    <link href="css/bootstrap.min.css" type="text/css" rel = "stylesheet"/>
    <link href="css/bootstrap-theme.min.css" type="text/css" rel = "stylesheet"/>
    <link href="css/bootstrap.min.css.map" type="text/css" rel = "stylesheet"/>
    <link href="css/style.css" type = "text/css" rel = "stylesheet"/>
</head>
<body>
<?php include_once ROOT . "/views/templates/header.php"?>

<div class="container content">
    <div class="row usluga" style="text-align: left">
        <div class="zagolovok"><h1>Английский язык для дошкольников и учащихся начальной школы</h1></div>
        <div class="col-md-6">
            <img src="img/english-desc.jpg">
        </div>
        <div class="col-md-6">
            <p>
                С каждым годом повышается роль английского языка в современном мире. И чтобы лучше овладеть навыками в его обучении, лучше начать его изучение с младшего дошкольного возраста.
            </p>
            <p>
                В нашем центре при обучении на курсах английского языка для дошкольников с 2,5 - 3 лет до 6 лет используется игровая методика.
            </p>
            <ul>
                <li style="text-decoration: none">1-я ступень обучения английского для детей-дошкольников 3-4 лет, где дети учатся слышать и воспринимать английскую речь на слух. Целью занятий является восприятие английского языка на слух, понимание и использование в своей речи английских слов и фраз.</li>
                <li style="text-decoration: none">2-я ступень обучения английского для детей 5-7 лет. Дети полностью погружаются в языковую среду. Играют, поют и общаются на занятиях на английском языке.</li>
                <li style="text-decoration: none">3-я ступень обучения английского для детей 6-10 лет. Если ваш ребенок закончил 1-4 классы начальной школы, но имеет пробелы в обучении, мы поможем решить эту проблему.</li>
            </ul>
        </div>
    </div>
    <div class="row desc-info-block">
        <div class="col-md-4">
            <img src="img/ruble.png">
            <h3>Стоимость</h3>
            <p>500 рублей за одно занятие</p>
        </div>
        <div class="col-md-4">
            <img src="img/clock.png">
            <h3>Продолжительность</h3>
            <p>1 час</p>
        </div>
        <div class="col-md-4">
            <img src="img/info.png">
            <h3>Дополнительно</h3>
            <p>Занятия ведутся индивидуально.</p>
        </div>
    </div>
</div>

<?php include_once ROOT . "/views/templates/enrollment-block.php"?>

<?php include_once ROOT . "/views/templates/footer.php"?>

</body>
</html>


