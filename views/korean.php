<!DOCTYPE html>
<html>
<head>
    <title>Курсы корейскрнр языка в городе Артёме</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="Наш центр предлагает изучение корейского  языка, как для младших школьников, так и для взрослых. Мы организуем обучение под Вас, что позволит Вам изучить язык с разным уклоном на грамматику, лексику, чтение, деловую лексику, разговорный">
    <meta name="Keywords" content="Галилео, учебный центр, ЕГЭ город Артем">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Pacifico" rel="stylesheet">
    <link href="css/bootstrap.min.css" type="text/css" rel = "stylesheet"/>
    <link href="css/bootstrap-theme.min.css" type="text/css" rel = "stylesheet"/>
    <link href="css/bootstrap.min.css.map" type="text/css" rel = "stylesheet"/>
    <link href="css/style.css" type = "text/css" rel = "stylesheet"/>
</head>
<body>
<?php include_once ROOT . "/views/templates/header.php"?>

<div class="container content">
    <div class="row usluga" style="text-align: left">
        <div class="zagolovok"><h1>Курсы корейского языка</h1></div>
        <div class="col-md-6">
            <img src="img/korean-desc.jpg">
        </div>
        <div class="col-md-6">
            <p>
                Изучение языков  — хорошая инвестиция.
            </p>
            <p>
                Наш центр предлагает изучение корейского  языка, как для младших школьников, так и для взрослых. Мы организуем обучение под Вас, что позволит Вам изучить язык с разным уклоном на грамматику, лексику, чтение, деловую лексику, разговорный.
            </p>
        </div>
    </div>
    <div class="row desc-info-block">
        <div class="col-md-4">
            <img src="img/ruble.png">
            <h3>Стоимость</h3>
            <p>500 рублей за одно занятие</p>
        </div>
        <div class="col-md-4">
            <img src="img/clock.png">
            <h3>Продолжительность</h3>
            <p>1 час</p>
        </div>
        <div class="col-md-4">
            <img src="img/info.png">
            <h3>Дополнительно</h3>
            <p>Занятия проводятся индивидуально с каждым  учеником. Преподаватель имеет лицензию на образовательную деятельность и опыт подготовки учащихся  к сдаче международных экзаменов.</p>
        </div>
    </div>
</div>

<?php include_once ROOT . "/views/templates/enrollment-block.php"?>

<?php include_once ROOT . "/views/templates/footer.php"?>

</body>
</html>


