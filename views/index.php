<!DOCTYPE html>
<html>
    <head>
        <title>Учебный центр Галилео город Артем. Подготовка к ЕГЭ и ОГЭ по всем предметам школьной программы</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="description" content="Подготовка к ЕГЭ (11 кл.) и ОГЭ (9 кл.) по всем предметам школьной программы (математика, физика, русский язык, литература, история, обществознание)">
        <meta name="Keywords" content="Галилео, учебный центр, ЕГЭ город Артем">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lobster|Pacifico" rel="stylesheet">
        <link href="css/bootstrap.min.css" type="text/css" rel = "stylesheet"/>
        <link href="css/bootstrap-theme.min.css" type="text/css" rel = "stylesheet"/>
        <link href="css/bootstrap.min.css.map" type="text/css" rel = "stylesheet"/>
        <link href="css/style.css" type = "text/css" rel = "stylesheet"/>
    </head>
    <body>
        <?php include_once ROOT . "/views/templates/header.php"?>

        <div id="carousel-example-generic" class="carousel slide" style="max-width: 940px; margin: auto" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="img/ege.jpg" style="margin: auto" alt="First slide">
                </div>
                <div class="item">
                    <img src="img/korean.jpg" style="margin: auto" alt="Second slide">
                </div>
                <div class="item">
                    <img src="img/preschool.jpg" style="margin: auto" alt="Second slide">
                </div>
                <div class="item">
                    <img src="img/correction.jpg" style="margin: auto" alt="Second slide">
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="container content">
            <div class="zagolovok"><h1>Образовательные программы</h1></div>
            <div class="usluga">
                <div class="row">
                    <div class="uslugaImg col-md-4">
                        <a><img src="img/ege2.png"></a>
                        <p><span style="color: #2e6da4; font-weight: bold">Подготовка к ЕГЭ  и ОГЭ</span><br>Подготовка к ЕГЭ (11 кл.) и ОГЭ (9 кл.) по всем предметам школьной программы.</p>
                        <a class="btn" href="podgotovka-k-ege">3000 руб. / месяц (8 занятий)</a>
                    </div>
                    <div class="uslugaImg col-md-4">
                        <a><img src="img/preschool2.png"></a>
                        <p><span style="color: #2e6da4; font-weight: bold">Дошкольное образование</span><br> Развитие детей дошкольного возраста (4-5, 5-6 и 6-7 лет).</p>
                        <a class="btn" href="preschool">500 руб. / час</a>
                    </div>
                    <div class="uslugaImg col-md-4">
                        <a><img src="img/korean2.png"></a>
                        <p><span style="color: #2e6da4; font-weight: bold">Корейский язык</span><br>Изучение корейского языка для детей и взрослых.</p>
                        <a class="btn" href="korean">500 руб. / час</a>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="uslugaImg col-md-6">
                        <a><img src="img/correction2.png"></a>
                        <p><span style="color: #2e6da4; font-weight: bold">Коррекция школьных знаний</span><br>Ликвидация пробелов в знаниях по всем предметам начальной школы.</p>
                        <a class="btn" href="correction">500 руб. / час</a>
                    </div>
                    <div class="uslugaImg col-md-6">
                        <a><img src="img/english.png"></a>
                        <p><span style="color: #2e6da4; font-weight: bold">Английский язык</span><br>Английский язык для дошкольников и учащихся начальной школы.</p>
                        <a class="btn" href="english">500 руб. / час</a>
                    </div>
                </div>
                <h3 style="color: #2e6da4;font-style: italic;">При выборе 2-х и более курсов предоставляется 15 % скидка на каждый курс!</h3>
            </div>

            <div class="zagolovok"><h1>О нас</h1></div>
            <div class="row usluga" style="padding: 10px;">
                <div class="col-md-5">
                    <img src="img/atom.png" alt="">
                </div>
                <div class="col-md-7" style="font-size: 16px">
                    <h1>Учебно-консультационный центр «Галилео»</h1><br>
                    <p>
                        Учебно-консультационный центр «Галилео»  - это частное учреждение дополнительного образования детей дошкольного и школьного возраста.
                    </p>
                    <p>
                        В учреждении работают высококвалифицированные педагоги  со стажем работы свыше 25 лет.
                        Мы умеем работать и любим свое дело. Занятия проводятся в мини-группах, что дает возможность педагогу работать с каждым учеником индивидуально, но при этом присутствует и коллективное обучение, что порождает здоровую конкуренцию. А это благотворно сказывается на учебном процессе. Подобные занятия имеют системный подход. По статистике почти 50% всех абитуриентов выбирает именно такой способ подготовки к госэкзамену.
                        Так же мы имеем опыт подготовки к внутренним экзаменам, являющимися обязательными для некоторых высших учебных заведений.
                    </p>
                    <p>
                        Мы дорожим репутацией, и результаты сдачи наших учеников  ЕГЭ и ОГЭ подтверждают эффективность  нашей работы. Все выпускники, которые обучались в нашем центре успешно сдали экзамены.
                    </p>
                </div>
            </div>

            <div class="zagolovok"><h1>Как нас найти</h1></div>
            <div class="row usluga">
                <div class="col-md-4" style="padding: 10% 0;">
                    <br>
                    <p>Мы находимся по адресу:<br> г.Артём, ул.Лазо 11</p>
                    <p>Телефон: +7 (924) 122-61-10</p>
                    <p>Электронная почта: pak-mun@yandex.ru</p>
                </div>
                <div class="col-md-8">
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d725.2268503032243!2d132.1899248319022!3d43.35804749721449!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5fb3b044e9e36fe5%3A0x189ad2d883fdc2a!2z0YPQuy4g0JvQsNC30L4sIDExLCDQkNGA0YLQtdC8LCDQn9GA0LjQvNC-0YDRgdC60LjQuSDQutGA0LDQuSwgNjkyNzYw!5e0!3m2!1sru!2sru!4v1491738344816" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once ROOT . "/views/templates/footer.php"?>
    </body>
</html>

