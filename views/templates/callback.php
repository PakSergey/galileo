<div class="modal fade" id="modal_callback">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 style="text-align: center;color: #6eb500"><img src="img/support.png" style="width: 50px;height: 50px; margin-right: 10px">Обратный звонок</h3>
            </div>
            <div class="modal-body">
                <form class="form-group" id="callback-form" style="text-align: center">
                    <label for="name">Ваше имя</label><input id="name" name="name" type="text" class="form-control" style="width: 50%; margin: auto" required>
                    <label for="phone">Телефон</label><input type="tel" id="phone" name="phone" required class="form-control"  style="width: 50%; margin: auto"><br>
                    <button type="submit" class="btn" id="submit_callback_form">заказать звонок</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">закрыть</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_success">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 style="text-align: center; color: green">Заявка отправлена!</h3>
            </div>
            <div class="modal-body" style="text-align: center;font-size: 18px;font-family: 'Exo 2', sans-serif;">
                <p>Спасибо за обращение! Наши сотрудники свяжутся с вами в течение дня.</p>
            </div>
        </div>
    </div>
</div>