<nav class="navbar navbar-default header">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/" style="padding: 0;margin-top: 5px;margin-right: 10px;">
                <img alt="Brand" src="img/logo.png">
            </a>
        </div>

        <div>
            <ul class="nav navbar-nav collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <li><a href="/" <?php if ($page == "main") echo "class='active-link'"?>>Главная</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle <?php if ($page != "main") echo "active-link"?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Курсы<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="podgotovka-k-ege" <?php if ($page == "ege") echo "class='active-link'"?>>Подготовка к ЕГЭ и ОГЭ</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="preschool" <?php if ($page == "preschool") echo "class='active-link'"?>>Дошкольное образование</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="korean" <?php if ($page == "korean") echo "class='active-link'"?>>Курсы корейского языка</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="correction" <?php if ($page == "correction") echo "class='active-link'"?>>Коррекция школьных знания</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="english" <?php if ($page == "english") echo "class='active-link'"?>>Английский язык</a></li>
                        <li role="separator" class="divider"></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right" style="margin-top: 8px;font-size: 16px;font-family: 'Exo 2', sans-serif;">
                <li style="text-align: center">г.Артём ул.Лазо 11 <br> +7 (924) 122-61-10</li>
                <li style="text-align: center"><button class="btn" style="font-size: 18px" data-toggle="modal" id="callback_btn" data-target="#modal_callback">Заказать звонок</button></li>
            </ul>
        </div>
    </div>
</nav>

<?php include_once ROOT . "/views/templates/callback.php"?>
