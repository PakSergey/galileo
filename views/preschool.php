<!DOCTYPE html>
<html>
<head>
    <title>Подготовка к школе в городе Артёме</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="Школа - важный период в жизни ребенка и чем лучше он будет к ней готов, тем легче будет потом учиться.">
    <meta name="Keywords" content="Галилео, учебный центр, ЕГЭ город Артем">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Pacifico" rel="stylesheet">
    <link href="css/bootstrap.min.css" type="text/css" rel = "stylesheet"/>
    <link href="css/bootstrap-theme.min.css" type="text/css" rel = "stylesheet"/>
    <link href="css/bootstrap.min.css.map" type="text/css" rel = "stylesheet"/>
    <link href="css/style.css" type = "text/css" rel = "stylesheet"/>
</head>
<body>
<?php include_once ROOT . "/views/templates/header.php"?>

<div class="container content">
    <div class="row usluga" style="text-align: left">
        <div class="zagolovok"><h1>Дошкольное образование</h1></div>
        <div class="col-md-6">
            <img src="img/preschool-desc.jpg">
        </div>
        <div class="col-md-6">
            <p>
                Школа - важный период в жизни ребенка и чем лучше он будет к ней готов, тем легче будет потом учиться.
            </p>
            <p>
                В программу обучения входит:
            </p>
            <ul>
                <li>развитие речи</li>
                <li>грамота</li>
                <li>письмо</li>
                <li>математика</li>
            </ul>
            <p>
                Наша подготовка к школе — это:
            </p>
            <ul>
                <li>качественное и эффективное обучение</li>
                <li>проверенные методики</li>
                <li>удобное расписание</li>
                <li>индивидуальный подход</li>
                <li>обучение с любого стартового уровня</li>
            </ul>
        </div>
    </div>
    <div class="row desc-info-block">
        <div class="col-md-4">
            <img src="img/ruble.png">
            <h3>Стоимость</h3>
            <p>500 рублей за одно занятие</p>
        </div>
        <div class="col-md-4">
            <img src="img/clock.png">
            <h3>Продолжительность</h3>
            <p>50 минут</p>
        </div>
        <div class="col-md-4">
            <img src="img/info.png">
            <h3>Дополнительно</h3>
            <p>Занятия проходят 2 раза в неделю. Курс ведет опытный педагог, имеющий стаж более 30 лет в образовательных учреждениях.</p>
        </div>
    </div>
</div>

<?php include_once ROOT . "/views/templates/enrollment-block.php"?>

<?php include_once ROOT . "/views/templates/footer.php"?>

</body>
</html>


