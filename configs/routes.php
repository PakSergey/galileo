<?php
return array(
    '/podgotovka-k-ege' => 'index/ege',
    '/preschool'        => 'index/preschool',
    '/korean'           => 'index/korean',
    '/correction'       => 'index/correction',
    '/english'          => 'index/english',
    '/callback'         => 'index/callback',
    '/'                  => 'index/index',
);
