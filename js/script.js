$( document ).ready(function() {
    $("#phone").mask("8(999) 999-9999");
    $("#callback-form").submit(function (event) {
        event.preventDefault();
        $.post(
            "/callback",
            {
                name: $("#name").val(),
                phone: $("#phone").val()
            },
            function () {
                $("#callback_btn").attr("data-target", "#modal_success");
                $("#modal_callback").modal("hide");
                $("#modal_success").modal("show");
            }
        );
    })
});
