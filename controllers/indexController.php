<?php

class indexController
{
    public function indexAction() {
        $page = "main";
        require_once ROOT . '/views/index.php';
    }

    public function egeAction() {
        $page = "ege";
        require_once ROOT . '/views/ege.php';
    }

    public function preschoolAction() {
        $page = "preschool";
        require_once ROOT . '/views/preschool.php';
    }

    public function koreanAction() {
        $page = "korean";
        require_once ROOT . '/views/korean.php';
    }

    public function correctionAction() {
        $page = "correction";
        require_once ROOT . '/views/correction.php';
    }

    public function englishAction() {
        $page = "english";
        require_once ROOT . '/views/english.php';
    }

    public function callbackAction() {
        if (isset($_POST["name"]) && isset($_POST["phone"])) {
            mail("galileo.artem@gmail.com", "Заявка с сайта", "Имя:".$_POST["name"].". Телефон: ".$_POST["phone"]);
        }
    }
}