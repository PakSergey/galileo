<?php
class Router
{
    public $routes;

    public function __construct()
    {
        $this->routes = include(ROOT.'/configs/routes.php');
    }

    private function  getURI(){
        if(!empty($_SERVER['REQUEST_URI']))
            return $_SERVER['REQUEST_URI'];
    }

    public function Start()
    {
        $uri = $this->getURI();
        foreach ($this->routes as $pattern=>$path) {
            if(preg_match("`$pattern`",$uri)) {
                $endingpath = preg_replace("`$pattern`",$path,$uri);
                $segemetns = explode('/',$endingpath);
                $controllername = array_shift($segemetns).'Controller';
                $actionname = array_shift($segemetns).'Action';
                $parameter=array_shift($segemetns);
                if(file_exists(ROOT.'/controllers/'.$controllername.'.php'))
                    include_once ROOT.'/controllers/'.$controllername.'.php';
                $controller=new $controllername();
                $controller->$actionname($parameter);
                break;
            }
        }

    }
}