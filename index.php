<?php
define('ROOT', __DIR__);

ini_set('error_reporting', 0);
ini_set('display_errors', 0);

require_once ROOT.'/classes/Router.php';

$router = new Router();
$router->Start();
